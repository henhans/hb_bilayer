# Check the density of state, band structure and the GA objects
import sys
import os
import numpy as np
from math import *
import h5py
sys.path.append(os.path.join(os.path.dirname("__file__"), "/home/henhans/WIEN_GUTZ/bin/tools/Gutzwiller"))
from dataproc import get_csr_matrix
from my_impurity_analysis import *
np.set_printoptions(precision=5, suppress=True)
from get_dos import *
from pylab import *
import matplotlib.pyplot as plt
import matplotlib

U=8.0
tp=0.09
filename = "glogbandU"+str(U)+"tp"+str(tp)+".h5"
get_dos(filename,U,tp,True)

f = h5py.File(filename,'r')
kps = np.linspace(-pi,pi,1000)
bnd_ek = f["/BND_EK"][0]
EF = f["/E_FERMI"][0]
#print bnd_ek
plt.plot(bnd_ek-EF)
plt.show()

print "E_tot=", f["/E_TB_TOT"][0]

print "error=",f["/GA_MAX_ERR"][0]
print "E_F=",EF
print "ntot=",get_n11_dimer_bilayer(filename) + get_n22_dimer_bilayer(filename)
R = f["Impurity_1/GA_R"][...].T
print "R="
print R
Z = np.dot(np.conj(R).T,R)

print "Z="
print Z

print "Lambda="
lam = f["Impurity_1/GA_La"][...]
print lam

print "n11=",get_n11_dimer_bilayer(filename)
print "n22=",get_n22_dimer_bilayer(filename)
print "n1up=",get_n1up_dimer_bilayer(filename)
print "n1dn=",get_n1dn_dimer_bilayer(filename)
print "n2up=",get_n2up_dimer_bilayer(filename)
print "n2dn=",get_n2dn_dimer_bilayer(filename)

print "D="
print f["Impurity_1/GA_D"][...]
print "Lambda_c="
print f["Impurity_1/GA_Lc"][...]

f.close()

# Check the Gauge transformation
from scipy import *

print "Lambda eigenvalues="
print eigvals(lam[:,:])
u = eigh(lam[:,:])[1]

print "Lambda diagonalize="
print np.dot( np.conj(u).T, np.dot( lam, u ) )

print "Transformed R="
print np.dot( np.conj(u).T, R )

print "u="
print u

print np.dot( np.conj(np.dot( np.conj(u).T, R )).T, np.dot( np.conj(u).T, R ) )

# Calculating and print density matrix
from scipy.sparse import csr_matrix
from dataproc import get_csr_matrix, get_ed_lowest_vec, trace_single_state

filename = "glogbandU"+str(U)+"tp"+str(tp)+".h5"
f = h5py.File(filename, 'r')

rho = get_csr_matrix(f, "/Impurity_1/RHO")
density_matrix = zeros((4,4),dtype=complex)
for i in range(4):
        for j in range(4):
            ci = get_csr_matrix(f, "/Impurity_1/annihi.op._" + str(i+1))
            cj = get_csr_matrix(f, "/Impurity_1/annihi.op._" + str(j+1))
            ci_dagger_cj = ci.getH() * cj
            density_matrix[i,j] = np.sum((rho * ci_dagger_cj).diagonal())
print "Density matrix="
print density_matrix
