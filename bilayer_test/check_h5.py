import sys
import os
import numpy as np
import subprocess as sp
import shutil
import h5py
sys.path.append(os.path.join(os.path.dirname(__file__), "/home/henhans/WIEN_GUTZ/bin/tools/Gutzwiller"))
from dataproc import get_csr_matrix
from tp_1D_bilayer_Model import *
from impurity_analysis import get_ed_d_occ_1

d_list = []; Z_list = []; e_int_list = []; e_tot_list = []

# Check convergence
f = h5py.File("glog.h5", 'r')
max_err = f["/GA_MAX_ERR"][0]
if max_err > .001:
  # Try the default initial input.
  os.remove("WH_RLNEF.INP")
  # Execute CyGutz
  sp.call('${WIEN_GUTZ_ROOT}/CyGutz', shell=True)
f.close()

# Save the Gutzwiller solution for the next as better initial input.
shutil.copyfile("WH_RLNEF.OUT", "WH_RLNEF.INP")
# Save the glog for latter data analysis
#fname = 'glogU'+str(U)+'tp'+str(tp)+'.h5'
#shutil.copyfile('glog.h5',fname)

# Store double occupancy, Z, e_int and e_tot
f = h5py.File("glog.h5", 'r')

# Interaction energy
e_int_list.append(f["/E_POT2_U"][0])
print 'f["/E_POT2_U"][0]=',f["/E_POT2_U"][0]

# Total energy
e_tot_list.append(f["/E_TB_TOT"][0])

# Z=R^\dagger R, proportional to identity in this case
R = f["/Impurity_1/GA_R"][...].T; Z = np.dot(np.conj(R).T,R)
Z_list.append(Z[2,2].real)

f.close()

# Double occupancy, the fourth (one-based) diagonal element in the local reduced density matrix \rho ( = \phi \phi^(\dagger)) 
# in this case. Note the order of the Fock basis is |0>, |1down>, |1up>, |1down, 1up>.
# Read \phi in sparse matrix format
#rho = get_csr_matrix(f, "/Impurity_1/RHO")
double_occ = get_ed_d_occ_1()
print 'double occupancy=',double_occ
d_list.append(double_occ) # python uses zero-based convention.

