import sys
import os
import numpy as np
import subprocess as sp
import shutil
import h5py
sys.path.append(os.path.join(os.path.dirname(__file__), "/home/thlee/WIEN_GUTZ/bin/tools/Gutzwiller"))
from dataproc import get_csr_matrix
from tp_1D_bilayer_Model import *
from impurity_analysis import get_d11_dimer_bilayer, get_ed_d_occ_1, get_n11_dimer_bilayer, get_n22_dimer_bilayer

def scan_Utp_1D_1_band_Hubbard_Model(U_lower_bound, U_upper_bound, U_step, tp_lower_bound, tp_upper_bound, tp_step):
  '''
  In this script, you will learn:

  * Perform Gutzwiller calculations with U in range(U_lower_bound, U_upper_bound, U_step),
  and tp in range(tp_lower_bound, tp_upper_bound, tp_step).
  * Store the key quantities, including double_occupany d, Z(=R^\dagger R), interaction 
  energy e_int, total energy e_tot as a function of U, and generate figures for them.
  Please click [source] on upper right to see detailed notes about the script. Note that here we have only one impurity.
  Type the following line to run the script::
    $ python ${WIEN_GUTZ_ROOT}/examples/1_1D_1_band_Hubbard_Model/scan_1D_1_band_Hubbard_Model.py

  '''
  # Prepare some lists to store the quantities of interest.
  #d_list = []; Z_list = []; e_int_list = []; e_tot_list = []

  # Make a copy of GL.INP to GL.INP_TEMPLATE, since we will modify the U value in that file.
  #shutil.copyfile("V2AO.INP", "V2AO.INP_TEMPLATE")

  # Get the content (line with U) to be modified.
  #target_str_list = []
  #with open("V2AO.INP_TEMPLATE", 'r') as f:
  #  for line in f.readlines():
  #    if '8.0000000000' in line:
  #      target_str_list.append(line[line.index('8.0000000000'):])

  # begin loop over U
  U_list = np.arange(U_lower_bound, U_upper_bound, U_step)
  tp_list = np.arange(tp_lower_bound, tp_upper_bound, tp_step)
  for tp in tp_list:
    # Prepare some lists to store the quantities of interest.
    d_list = []; Z_list = []; e_int_list = []; e_tot_list = []; U_accum = []; n_tot_list = []; efermi_list=[]

    tutorial_2_1D_bilayer_Model(tp=tp)
    shutil.copyfile("WH_RLNEF.BEST", "WH_RLNEF.INP")
    for U in U_list:
      print "In scan Utp tp=",tp," U=", U
      # Modify parameter U in V2AO.INP
      with open("V2AO.INP_TEMPLATE", 'r') as source:
        with open("V2AO.INP", 'w') as target:
            #for target_str in target_str_list:
            #    target.write(source.read().replace(target_str, str(U) + '\n'))
            for line in source.readlines():
                target.write(line.replace('U', str(U))) 
    
      try:
          os.remove("GLU2_1.TMP")
          #os.remove("WH_RLNEF.INP")
      except:
          pass
    
      # Execute CyGutz
      sp.call('${WIEN_GUTZ_ROOT}/CyGutz', shell=True)
    
      # Check convergence
      f = h5py.File("glog.h5", 'r')
      max_err = f["/GA_MAX_ERR"][0]
      if max_err > .001:
        # Try the default initial input.
        os.remove("WH_RLNEF.INP")
        try:
            os.remove("GLU2_1.TMP")
        except:
            pass
        # Execute CyGutz
        sp.call('${WIEN_GUTZ_ROOT}/CyGutz', shell=True)
      f.close()
    
      # Save the Gutzwiller solution for the next as better initial input.
      shutil.copyfile("WH_RLNEF.OUT", "WH_RLNEF.INP")
      # Save the glog for latter data analysis
      fname = 'glogU'+str(U)+'tp'+str(tp)+'.h5'
      shutil.copyfile('glog.h5',fname)
    
      # Store double occupancy, Z, e_int and e_tot
      f = h5py.File("glog.h5", 'r')
    
      # Interaction energy
      e_int_list.append(f["/E_POT2_U"][0])
    
      # Total energy
      e_tot_list.append(f["/E_TB_TOT"][0])
    
      # Z=R^\dagger R, proportional to identity in this case
      R = f["/Impurity_1/GA_R"][...].T; Z = np.dot(np.conj(R).T,R)
      Z_list.append(Z[2,2].real)

      # fermi level
      efermi_list.append( f["/E_FERMI"][0] )
     
      # store interaction U
      U_accum.append(U) 
      f.close()
      
      # Double occupancy, the fourth (one-based) diagonal element in the local reduced density matrix \rho ( = \phi \phi^(\dagger)) 
      # in this case. Note the order of the Fock basis is |0>, |1down>, |1up>, |1down, 1up>.
      # Read \phi in sparse matrix format
      #rho = get_csr_matrix(f, "/Impurity_1/RHO")
      double_occ = get_d11_dimer_bilayer()#get_ed_d_occ_1()#get_d11_sb()
      print 'double occupancy=',double_occ,' Z=',Z[2,2]
      d_list.append(double_occ) # python uses zero-based convention.

      # total occupation number
      occupation = get_n11_dimer_bilayer() + get_n22_dimer_bilayer() 
      n_tot_list.append( occupation )
      print 'occupantion=', occupation
      
      print "Z=",Z[2,2]
      if Z[2,2] < 0.02:
          break
    
    print 'plotting.... '
    # Use matplotlib to plot figures.
    import matplotlib
    # Good if no display is installed
    matplotlib.use('agg')
    import matplotlib.pyplot as plt
    fname = 'tp='+str(tp)
    #plt.title(fname)
    f, axarr = plt.subplots(3, 2, sharex = True)
    axarr[0, 0].set_title(fname)
    axarr[0, 0].plot(U_accum, d_list, '-o')
    axarr[0, 0].set_ylabel('double occupancy')
    axarr[0, 0].set_xlabel('U/t')
    axarr[0, 1].plot(U_accum, Z_list, '-o')
    axarr[0, 1].set_ylabel('Z')
    axarr[0, 1].set_xlabel('U/t')
    axarr[0, 1].yaxis.tick_right()
    axarr[0, 1].yaxis.set_label_position("right")
    axarr[1, 0].plot(U_accum, e_int_list, '-o')
    axarr[1, 0].set_ylabel('interaction_energy/t')
    axarr[1, 0].set_xlabel('U/t')
    axarr[1, 1].plot(U_accum, e_tot_list, '-o')
    axarr[1, 1].set_ylabel('total_energy/t')
    axarr[1, 1].set_xlabel('U/t')
    axarr[1, 1].yaxis.tick_right()
    axarr[1, 1].yaxis.set_label_position("right")
    axarr[2, 0].plot(U_accum, n_tot_list, '-o')
    axarr[2, 0].set_ylabel('ntot=n1+n2')
    axarr[2, 0].set_xlabel('U/t')
    axarr[2, 1].plot(U_accum, efermi_list, '-o')
    axarr[2, 1].set_ylabel('fermi_energy/t')
    axarr[2, 1].set_xlabel('U/t')
    axarr[2, 1].yaxis.tick_right()
    axarr[2, 1].yaxis.set_label_position("right")
 
    fname = '1D_bilayer_ZEUtp'+str(tp)+'.png'
    plt.savefig(fname)
  
    #print 'U',U_list
    #print 'Z',Z_list
    #print 'd',d_list

if __name__=="__main__":
<<<<<<< HEAD
  scan_Utp_1D_1_band_Hubbard_Model(0.0, 12.2, 0.2, 1.20, 1.30, 0.2)
=======
  scan_Utp_1D_1_band_Hubbard_Model(0.0, 12.2, 0.2, 1.2, 1.30, 0.1)
>>>>>>> c0515e0f7ac67857a43def04f32d2ca0d286ff95
